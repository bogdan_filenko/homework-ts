import { fight } from './fight';
import { createFighter } from './fighterView';
import { showWinnerModal } from './modals/winner';
import { createElement } from './helpers/domHelper';
import { getFighterDetails } from './services/fightersService';
import { showFighterDetailsModal } from './modals/fighterDetails';
export function createFighters(fighters) {
    const selectFighterForBattle = createFightersSelector();
    const fighterElements = fighters.map((fighter) => createFighter(fighter, showFighterDetails, selectFighterForBattle));
    const fightersContainer = createElement({ tagName: 'div', className: 'fighters' });
    fightersContainer.append(...fighterElements);
    return fightersContainer;
}
async function showFighterDetails(event, fighter) {
    try {
        const fullInfo = await getFighterInfo(fighter._id);
        showFighterDetailsModal(fullInfo);
    }
    catch (error) {
        console.warn(error);
    }
}
async function getFighterInfo(fighterId) {
    try {
        const fighterInfo = await getFighterDetails(fighterId);
        return fighterInfo;
    }
    catch (error) {
        throw error;
    }
}
function createFightersSelector() {
    const selectedFighters = new Map();
    return async function selectFighterForBattle(event, fighter) {
        try {
            const fullInfo = await getFighterInfo(fighter._id);
            if (event?.target.checked) {
                selectedFighters.set(fighter._id, fullInfo);
            }
            else {
                selectedFighters.delete(fighter._id);
            }
            if (selectedFighters.size === 2) {
                const winner = fight(...Array.from(selectedFighters.values()));
                showWinnerModal(winner);
            }
        }
        catch (error) {
            console.warn(error);
        }
    };
}
