export function fight(...fighters) {
    let firstFighter = copyFighterObject(fighters[0]);
    let secondFighter = copyFighterObject(fighters[1]);
    let winner = undefined;
    while (!winner) {
        firstFighter.health -= getDamage(secondFighter, firstFighter);
        if (firstFighter.health < 0) {
            winner = secondFighter;
            break;
        }
        secondFighter.health -= getDamage(firstFighter, secondFighter);
        if (secondFighter.health < 0) {
            winner = firstFighter;
        }
    }
    return winner;
}
export function getDamage(attacker, enemy) {
    const hitPower = getHitPower(attacker);
    const blockPower = getBlockPower(enemy);
    let damage = 0;
    if (hitPower > blockPower) {
        damage = hitPower - blockPower;
    }
    return damage;
}
export function getHitPower(fighter) {
    const { attack } = fighter;
    const criticalHitChance = 1 + Math.random();
    const hitPower = attack * criticalHitChance;
    return hitPower;
}
export function getBlockPower(fighter) {
    const { defense } = fighter;
    const dodgeChance = 1 + Math.random();
    const blockPower = defense * dodgeChance;
    return blockPower;
}
function copyFighterObject(source) {
    return {
        _id: source._id,
        name: source.name,
        health: source.health,
        attack: source.attack,
        defense: source.defense,
        source: source.source
    };
}
