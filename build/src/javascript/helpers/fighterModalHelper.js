import { createElement } from "./domHelper";
export function createFighterModal(fighter) {
    const fighterModal = createElement({ tagName: 'div', className: 'modal-body' });
    const fighterModalComponents = createFighterModalComponents(fighter);
    fighterModal.append(...fighterModalComponents);
    return fighterModal;
}
function createFighterModalComponents(fighter) {
    const { name, source, health, attack, defense } = fighter;
    const imageAttributes = {
        src: source
    };
    const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: imageAttributes });
    const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
    const healthElement = createElement({ tagName: 'span', className: 'fighter-health' });
    const attackElement = createElement({ tagName: 'span', className: 'fighter-attack' });
    const defenceElement = createElement({ tagName: 'span', className: 'fighter-defence' });
    nameElement.innerText = name;
    healthElement.innerText = health.toString();
    attackElement.innerText = attack.toString();
    defenceElement.innerText = defense.toString();
    return [
        imageElement,
        nameElement,
        healthElement,
        attackElement,
        defenceElement
    ];
}
