export function createElement(elementData) {
    const element = document.createElement(elementData.tagName);
    if (elementData.className) {
        element.classList.add(elementData.className);
    }
    if (elementData.attributes) {
        Object.keys(elementData.attributes).forEach((key) => element.setAttribute(key, elementData.attributes[key]));
    }
    return element;
}
