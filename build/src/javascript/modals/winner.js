import { showModal } from "./modal";
import { createFighterModal } from "../helpers/fighterModalHelper";
export function showWinnerModal(fighter) {
    const title = 'Winner!!!!';
    const bodyElement = createFighterModal(fighter);
    showModal({ title, bodyElement });
}
