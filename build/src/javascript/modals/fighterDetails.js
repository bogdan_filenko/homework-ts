import { showModal } from './modal';
import { createFighterModal } from '../helpers/fighterModalHelper';
export function showFighterDetailsModal(fighter) {
    const title = 'Fighter info';
    const bodyElement = createFighterModal(fighter);
    showModal({ title, bodyElement });
}
