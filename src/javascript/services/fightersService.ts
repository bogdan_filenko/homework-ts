import { Fighter } from '../models/Fighter';
import { callApi } from '../helpers/apiHelper';
import { FighterDetail } from '../helpers/mockData';

export async function getFighters(): Promise<Fighter[]> {
  try {
    const endpoint: string = 'fighters.json';
    const apiResult: Fighter[] | FighterDetail = await callApi(endpoint, 'GET');
    
    return apiResult as Fighter[];
  } 
  catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id: number | string): Promise<FighterDetail> {
  try {
    const endpoint: string = `${id}.json`;
    const apiResult: Fighter[] | FighterDetail = await callApi(endpoint, 'GET');

    return apiResult as FighterDetail;
  }
  catch (error) {
    throw error;
  }
}