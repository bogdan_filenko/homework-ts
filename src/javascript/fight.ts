import { FighterDetail } from "./helpers/mockData";

export function fight(...fighters: FighterDetail[]): FighterDetail {
    let firstFighter: FighterDetail = copyFighterObject(fighters[0]);
    let secondFighter: FighterDetail = copyFighterObject(fighters[1]);
    
    let winner: FighterDetail | undefined = undefined;
    while (!winner) {
      firstFighter.health -= getDamage(secondFighter, firstFighter);
      if (firstFighter.health < 0) {
        winner = secondFighter;
        break;
      }

      secondFighter.health -= getDamage(firstFighter, secondFighter);
      if (secondFighter.health < 0) {
        winner = firstFighter;
      }
    }

    return winner as FighterDetail;
  }
  
  export function getDamage(attacker: FighterDetail, enemy: FighterDetail): number {
    const hitPower: number = getHitPower(attacker);
    const blockPower: number = getBlockPower(enemy);
    let damage: number = 0;

    if (hitPower > blockPower) {
      damage = hitPower - blockPower;
    }

    return damage;
  }
  
  export function getHitPower(fighter: FighterDetail): number {
    const { attack } = fighter;

    const criticalHitChance: number = 1 + Math.random();
    const hitPower: number = attack * criticalHitChance;

    return hitPower;
  }
  
  export function getBlockPower(fighter: FighterDetail): number {
    const { defense } = fighter;

    const dodgeChance: number = 1 + Math.random();
    const blockPower: number = defense * dodgeChance;

    return blockPower;
  }

  function copyFighterObject(source: FighterDetail): FighterDetail {
    return {
      _id: source._id,
      name: source.name,
      health: source.health,
      attack: source.attack,
      defense: source.defense,
      source: source.source
    };
  }

