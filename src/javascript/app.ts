import { Fighter } from './models/Fighter';
import { createFighters } from './fightersView';
import { getFighters } from './services/fightersService'

const rootElement = document.getElementById('root') as HTMLElement;
const loadingElement = document.getElementById('loading-overlay') as HTMLElement;

export async function startApp(): Promise<void> {
  try {
    loadingElement.style.visibility = 'visible';
    
    const fighters: Fighter[] = await getFighters();
    const fightersElement: HTMLElement = createFighters(fighters);

    rootElement.appendChild(fightersElement);
  }
  catch (error) {
    console.warn(error);
    rootElement.innerText = 'Failed to load data';
  }
  finally {
    loadingElement.style.visibility = 'hidden';
  }
}
