export interface FighterCharacteristic {
    health: number; 
    attack: number;
    defense: number;
}