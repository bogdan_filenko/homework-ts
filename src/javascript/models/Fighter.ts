export interface Fighter {
    _id: number | string;
    name: string;
    source: string;
}