import { showModal } from "./modal";
import { FighterDetail } from "../helpers/mockData";
import { createFighterModal } from "../helpers/fighterModalHelper";

export  function showWinnerModal(fighter: FighterDetail): void {
  const title: string = 'Winner!!!!';
  const bodyElement: HTMLElement = createFighterModal(fighter);

  showModal({ title, bodyElement });
}