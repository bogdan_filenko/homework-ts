import { showModal } from './modal';
import { FighterDetail } from '../helpers/mockData';
import { createFighterModal } from '../helpers/fighterModalHelper';

export function showFighterDetailsModal(fighter: FighterDetail): void {
  const title: string = 'Fighter info';
  const bodyElement: HTMLElement = createFighterModal(fighter);

  showModal({ title, bodyElement });
}
