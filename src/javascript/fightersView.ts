import { fight } from './fight';
import { Fighter } from './models/Fighter';
import { createFighter } from './fighterView';
import { showWinnerModal } from './modals/winner';
import { FighterDetail } from './helpers/mockData';
import { createElement } from './helpers/domHelper';
import { getFighterDetails } from './services/fightersService';
import { showFighterDetailsModal } from './modals/fighterDetails';

export function createFighters(fighters: Fighter[]): HTMLElement {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements: HTMLElement[] = fighters.map((fighter: Fighter) =>
    createFighter(fighter, showFighterDetails, selectFighterForBattle));
    
  const fightersContainer: HTMLElement = createElement({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

async function showFighterDetails(event: Event, fighter: Fighter): Promise<void> {
  try {
    const fullInfo: FighterDetail = await getFighterInfo(fighter._id);
    showFighterDetailsModal(fullInfo);
  }
  catch (error) {
    console.warn(error);
  }
}

async function getFighterInfo(fighterId: number | string): Promise<FighterDetail> {
  try {
    const fighterInfo: FighterDetail = await getFighterDetails(fighterId);
    return fighterInfo;
  }
  catch (error) {
    throw error;
  }
}

function createFightersSelector(): (event: Event, fighter: Fighter) => Promise<void> {
  const selectedFighters = new Map<number | string, FighterDetail>();

  return async function selectFighterForBattle(event: Event, fighter: Fighter): Promise<void> {
    try {
      const fullInfo: FighterDetail = await getFighterInfo(fighter._id);

      if ((event?.target as HTMLInputElement).checked) {
        selectedFighters.set(fighter._id, fullInfo);
      } 
      else { 
        selectedFighters.delete(fighter._id);
      }

      if (selectedFighters.size === 2) {
        const winner: FighterDetail = fight(...Array.from(selectedFighters.values()));
        showWinnerModal(winner);
      }
    }
    catch (error) {
      console.warn(error);
    }
  }
}