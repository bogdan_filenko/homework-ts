import { IDictionary } from "../interfaces/dictionary";

export type ElementData = { tagName: string, className?: string, attributes?: IDictionary }

export function createElement(elementData: ElementData): HTMLElement {
  const element: HTMLElement = document.createElement(elementData.tagName);

  if (elementData.className) {
    element.classList.add(elementData.className);
  }
  if (elementData.attributes) {
    Object.keys(elementData.attributes).forEach((key: string) => 
      element.setAttribute(key, (elementData.attributes as IDictionary)[key]));
  }  
  
  return element;
}