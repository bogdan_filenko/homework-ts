import { FighterDetail } from "./mockData";
import { createElement } from "./domHelper";
import { IDictionary } from "../interfaces/dictionary";

export function createFighterModal(fighter: FighterDetail): HTMLElement {
    const fighterModal: HTMLElement = createElement({ tagName: 'div', className: 'modal-body' });
    const fighterModalComponents: HTMLElement[] = createFighterModalComponents(fighter);

    fighterModal.append(...fighterModalComponents);

    return fighterModal;
    }

function createFighterModalComponents(fighter: FighterDetail): HTMLElement[] {
    const { name, source, health, attack, defense } = fighter;

    const imageAttributes: IDictionary = {
        src: source
    };

    const imageElement: HTMLElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: imageAttributes })
    const nameElement: HTMLElement = createElement({ tagName: 'span', className: 'fighter-name' });
    const healthElement: HTMLElement = createElement({ tagName: 'span', className: 'fighter-health' });
    const attackElement: HTMLElement = createElement({ tagName: 'span', className: 'fighter-attack' });
    const defenceElement: HTMLElement = createElement({ tagName: 'span', className: 'fighter-defence' });

    nameElement.innerText = name;
    healthElement.innerText = health.toString();
    attackElement.innerText = attack.toString();
    defenceElement.innerText = defense.toString();

    return [
        imageElement,
        nameElement,
        healthElement,
        attackElement,
        defenceElement
    ];
}